﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
    /**
     * Model class modeling a request towards https://api.exchangeratesapi.io/
     */
    public class CurrencyModel
    {
        /**
         * The first currency in the list. 
         */

        public string FirstCurrency { get; set; }

        /**
         * The second currency in the list
         */
        public string SecondCurrency { get; set; }

        /**
         * The amount of first currency
         */
        [Display(Name = "First currency")]
        public double TotalAmountFirstCurrency { get; set; }

        /**
         * The amount of first currency
         */
        [Display(Name = "Second currency")]
        public double TotalAmountSecondCurrency { get; set; }


    }
}
