﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyConverter.Http;
using CurrencyConverter.Models;
using CurrencyConverter.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CurrencyConverter.Pages
{
    public class ConvertModel : PageModel
    {
        private readonly HttpRequest httpRequest;
        private Dictionary<string, double> _cachedValues;

        [TempData]
        public string ResultInfo { get; set; }
        

        [BindProperty]
        public CurrencyModel CurencyModel { get; set; }

        [BindProperty]
        public IEnumerable<SelectListItem> Options
        {
            get; set;
        }
        //this is used for one purpose, to send same list as Options to javascript
        //Trying to extract Options didn't work. This is a workaround
        public List<string> CountryList {
            get; set;
        }
        public ConvertModel()
        {
            httpRequest = new HttpRequest();
            _cachedValues = null;
            CurencyModel = new CurrencyModel();
           
            Options = AllCurency.CurrencyName.Values.ToList().Select(o => new SelectListItem
            {
                Value = o,
                Text = o
            }).ToList();
            //this is used for one purpose, to send same list as Options to javascript
            //Trying to extract Options didn't work. This is a workaround
            CountryList = AllCurency.CurrencyName.Values.ToList();
            CountryList.Insert(0, "Select currency");
        }
        //user pressed the button for calculating from the first currency
        public async Task/*<IActionResult>*/ OnPostFirstCurrency()
        {
            await CalculaCurrencysAsync(IsFirstCurrency: true);
        }

        /**
         * calculate between two currencies
         * the parameter IsFirstCurrency tells the method if we are doing the calculation from first or second currency
         */
        public async Task CalculaCurrencysAsync(bool IsFirstCurrency)
        {
            
            if (_cachedValues == null)
            {
                _cachedValues = await httpRequest.BuildCacheAsync();
            }

            double firstRate;
            double secondRate;
            //get the key in dictionary, for example Sweden kronors return SEK
            string firstCurrencyKey = AllCurency.CurrencyName.FirstOrDefault(x => x.Value == CurencyModel.FirstCurrency).Key;
            //get the key in dictionary, for example Danish kronors return DK
            string secondCurrencyKey = AllCurency.CurrencyName.FirstOrDefault(x => x.Value == CurencyModel.SecondCurrency).Key;
            if (string.IsNullOrEmpty(firstCurrencyKey) || string.IsNullOrEmpty(secondCurrencyKey))
            {
                ResultInfo = "Both  currency's must be selected";
                return;
            }

            firstRate = _cachedValues.GetValueOrDefault(firstCurrencyKey);
            secondRate = _cachedValues.GetValueOrDefault(secondCurrencyKey);
            double quota = 0;
            if (firstRate != 0 && secondRate != 0)
            {
                if (IsFirstCurrency)
                {
                    quota = secondRate / firstRate;
                    CurencyModel.TotalAmountSecondCurrency = CurencyModel.TotalAmountFirstCurrency * quota;
                    ResultInfo = CurencyModel.TotalAmountFirstCurrency + " " + CurencyModel.FirstCurrency + " equal " + CurencyModel.TotalAmountSecondCurrency + " " + CurencyModel.SecondCurrency;
                }
                else
                {
                    quota = firstRate / secondRate;
                    CurencyModel.TotalAmountFirstCurrency = CurencyModel.TotalAmountSecondCurrency * quota;
                    ResultInfo = CurencyModel.TotalAmountSecondCurrency + " " + CurencyModel.SecondCurrency + " equal " + CurencyModel.TotalAmountFirstCurrency + " " + CurencyModel.FirstCurrency;

                }

            }
        }

        public async Task OnPostSecondCurrency()
        {
            await CalculaCurrencysAsync(IsFirstCurrency: false);
        }
    }
}