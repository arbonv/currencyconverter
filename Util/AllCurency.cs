﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Util
{
    public static class AllCurency
    {
        public static Dictionary<string, string> CurrencyName { get; set; } = InitiateCurrencys;

        /**
         * initiate the dictionary with all currency's 
         * first parameter is the currency code
         * second parameter is a description of currency code eg for SEK,the description is Swedish krona
         */
        public static Dictionary<string, string> InitiateCurrencys
        {
            get
            {
                if (CurrencyName == null)
                {
                    CurrencyName = new Dictionary<string, string>();
                    CurrencyName = new Dictionary<string, string>();
                    CurrencyName.Add("BGN", "Bulgarian lev");
                    CurrencyName.Add("NZD", "New Zealand dollar");
                    CurrencyName.Add("ILS", "Israeli new shekel");
                    CurrencyName.Add("RUB", "Russian ruble");

                    CurrencyName.Add("CAD", "Canadian dollar");

                    CurrencyName.Add("USD", "United States dollar");

                    CurrencyName.Add("CHF", "Swiss franc");

                    CurrencyName.Add("AUD", "Australian dollar");

                    CurrencyName.Add("JPY", "Japanese yen");

                    CurrencyName.Add("TRY", "Turkish lira");

                    CurrencyName.Add("HKD", "Hong Kong dollar");

                    CurrencyName.Add("MYR", "Malaysian ringgit");

                    CurrencyName.Add("HRK", "Croatian kuna");

                    CurrencyName.Add("CZK", "Czech koruna");

                    CurrencyName.Add("IDR", "Indonesian rupiah");

                    CurrencyName.Add("DKK", "Danish krone");

                    CurrencyName.Add("NOK", "Norwegian krone");

                    CurrencyName.Add("HUF", "Hungarian forint");

                    CurrencyName.Add("GBP", "British pound");

                    CurrencyName.Add("MXN", "Mexican peso");

                    CurrencyName.Add("THB", "Thai baht");

                    CurrencyName.Add("ISK", "Icelandic króna");

                    CurrencyName.Add("ZAR", "South African rand");

                    CurrencyName.Add("BRL", "Brazilian real");

                    CurrencyName.Add("SGD", "Singapore dollar");

                    CurrencyName.Add("PLN", "Polish złoty");

                    CurrencyName.Add("INR", "Indian rupee");

                    CurrencyName.Add("KRW", "South Korean won");

                    CurrencyName.Add("CNY", "Chinese yuan");

                    CurrencyName.Add("SEK", "Swedish krona");

                    CurrencyName.Add("EUR", "Euro");

                }
                return CurrencyName;
            }
        }
    }
}
