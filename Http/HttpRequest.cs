﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Http
{
    // class responsible for http request towards our api end point
    public class HttpRequest
    {
        private string baseUrl = "https://api.exchangeratesapi.io/latest";
        HttpClient client = new HttpClient();
        /**
         * Dictionary containing the currency and the ratio for this currency towards euro
         */
        Dictionary<string, double> currencyes = new Dictionary<string, double>();
        public HttpRequest()
        {
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Dictionary<string, double>> BuildCacheAsync()
        {
            //use eur as base currency and calculate currency's from the list with exchange rates. 
            string baseCurrency = "EUR";
            var url = "https://api.exchangeratesapi.io/latest?base="+ baseCurrency ;
            var response =  await client.GetStringAsync(url);
            JObject data = JObject.Parse(response);
            JObject rates =(JObject) data.GetValue("rates");
            //populate the dictionary from the json values we get from our api end point
            currencyes = rates.ToObject<Dictionary<string, double>>();
            //add euro also so we can compare with euro also
            currencyes.Add("EUR", 1);

            return currencyes;
        }

    }
}
